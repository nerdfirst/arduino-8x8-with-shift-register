# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Run an 8x8 LED Matrix using 3 pins!**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/NM7wXta8crM/0.jpg)](https://www.youtube.com/watch?v=NM7wXta8crM "Click to Watch")


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.